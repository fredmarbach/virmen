function code = test_tunnel
% test_tunnel   Code for the ViRMEn experiment test_tunnel.
%   code = test_tunnel   Returns handles to the functions that ViRMEn
%   executes during engine initialization, runtime and termination.


% Begin header code - DO NOT EDIT
code.initialization = @initializationCodeFun;
code.runtime = @runtimeCodeFun;
code.termination = @terminationCodeFun;
% End header code - DO NOT EDIT



% --- INITIALIZATION code: executes before the ViRMEn engine starts.
function vr = initializationCodeFun(vr)

% --- init NIDAQ ---
% read speed info from arduino

% --- init user GUI ---
% ask user to choose mouse
% basic parameters loaded for this mouse


% --- init transform variables ---
vr.transformFunction.xFactor = 1;
vr.transformFunction.rFactor = 1;
vr.transformFunction.lengthScaleFactor = 0.8;
vr.transformFunction.monitorWidth = 1*410; %630;
vr.transformFunction.monitorHeight = 1*235; %350;
vr.transformFunction.mouseHeight = 0.5;
vr.transformFunction.minDistance_forX = 220;
vr.transformFunction.minDistance_forY = 320;
vr.transformFunction.monitorAngle = 80;

% --- init trajectory plot ---
% X-position of the symbol on the screen (0 is center)
symbolXPosition = 0;
% Symbol size as fraction of the screen
vr.symbolSize = 0.02;
% Track minimum and maximum positions
vr.trackMinY = -10;
vr.trackMaxY = 310;
% Create a square symbol and assign color to it
vr.plot(1).x = [-1 1 1 -1 -1]*vr.symbolSize + symbolXPosition;
vr.plot(1).y = [-1 -1 1 1 -1]*vr.symbolSize;
vr.plot(1).color = [1 0 0];
vr.plot(1).window = 1; % display plot in the first window

% --- init progress plots ---
% trial history plot
% trajectory plot
% performance plot and psych curve

% --- init tower variables ---
vr.towers.t_yOffset = 20;
vr = updateTowerVisibility(vr,'init');


% --- RUNTIME code: executes on every iteration of the ViRMEn engine.
function vr = runtimeCodeFun(vr)


% --- update tower visibility ---
vr = updateTowerVisibility(vr,'updateVisibility');

% --- check if endOfTrial ---

% --- update trajectory plot ---
% Normalize animal�s y-position to range from -1 to 1 (the monitor range)
symbolYPosition = 2*(vr.position(2)-vr.trackMinY)/(vr.trackMaxY-vr.trackMinY) - 1;
% Update the y-position of the symbol
vr.plot(1).y = [-1 -1 1 1 -1]*vr.symbolSize + symbolYPosition;


% --- TERMINATION code: executes after the ViRMEn engine stops.
function vr = terminationCodeFun(vr)
