function code = tmaze_1
% tmaze_1   Code for the ViRMEn experiment tmaze_1.
%   code = tmaze_1   Returns handles to the functions that ViRMEn
%   executes during engine initialization, runtime and termination.


% Begin header code - DO NOT EDIT
code.initialization = @initializationCodeFun;
code.runtime = @runtimeCodeFun;
code.termination = @terminationCodeFun;
% End header code - DO NOT EDIT

% -------------------------------------------------------------------
% -------------------------------------------------------------------
% --- INITIALIZATION code: executes before the ViRMEn engine starts.
% -------------------------------------------------------------------
% -------------------------------------------------------------------
function vr = initializationCodeFun(vr)


% --- init nonGUI parameters ---
vr = virmen_initNonGUIParams(vr);

% --- init user GUI ---
% asks user to choose mouse/task
vr = virmen_initGUIParams(vr);
[vr.virmenGUI,vr.S] = virmen_updateGUI(vr.S,'init');
pauseUntilClick([5 1],'northwest');
[vr.virmenGUI,vr.S] = virmen_updateGUI(vr.S,'sync',vr.virmenGUI);

% --- init trial vectors ---
vr = towers_trials(vr,'updateAblauf');

% --- init towers ---
vr = towers_handling(vr,'init');
vr = towers_handling(vr,'prepareTrial');
vr = towers_handling(vr,'placeTowers');   
vr = towers_handling(vr,'makeAllInvisible');

% --- put mouse at starting position ---
vr.position(1) = 0; % x
vr.position(2) = vr.towers.mouseInitialY; % this is determined in 'placeTowers'
vr.position(4) = 0.0001; % tiny view angle to avoid weird bug

% --- init trajectory plot ---
vr = towers_trajectoryPlot(vr,'init');
vr = towers_trajectoryPlot(vr,'updateTowers');

% --- init trials plot ---
vr = towers_trialPlot(vr,'init');
vr = towers_trialPlot(vr,'update');

% --- init performance plot ---
vr = towers_performancePlot(vr,'init');

% --- init DAQ ---
vr.daq.deviceName = 'Dev1';
vr = towers_DAQ(vr,'init');

% --- assign photoDiode to monitor ---
vr.plot(1).window = vr.S.GUI.monitorNumber; % monitor



% -------------------------------------------------------------------
% -------------------------------------------------------------------
% --- RUNTIME code: executes on every iteration of the ViRMEn engine.
% -------------------------------------------------------------------
% -------------------------------------------------------------------
function vr = runtimeCodeFun(vr)

tic

% --- check if mouse crossed threshold ---
% if so set vr.tmp.endOfTrial == true
vr = towers_endOfTrial(vr,'checkIfEnd');

% --- store some data ---
% in effect this is data from the iteration that just ended
vr.data.pos(vr.iterations,:) = vr.position; % - vr.towers.mouseInitialY;
vr.data.dt(vr.iterations) = vr.dt;
vr.data.time(vr.iterations) = vr.timeElapsed;
vr.data.trialNumber(vr.iterations) = vr.tmp.currentTrial;
vr.data.trialEnded(vr.iterations) = vr.tmp.endOfTrial;
vr.data.towerAppeared(vr.iterations) = vr.towers.towerJustAppeared;
%vr.data.valveOpen(vr.iterations) = vr.daq.valveOpen;
%vr.data.rotary(vr.iterations) = vr....
     
% save variables of --trial that just ended-- (which is still 'currentTrial' at this point)
if vr.tmp.endOfTrial == true
    vr.data.trial.towerY{vr.tmp.currentTrial} = vr.towers.y;
    vr.data.trial.towerX{vr.tmp.currentTrial} = vr.towers.x;
    vr.data.trial.sides{vr.tmp.currentTrial} = vr.towers.sides;
    vr.data.trial.mouseInitialY(vr.tmp.currentTrial) = vr.towers.mouseInitialY;
    vr.data.trial.GUI{vr.tmp.currentTrial} = vr.S.GUI;
end


if vr.tmp.ITI == true
    % --- check if mouse is in ITI ---
    vr = towers_ITItunnel(vr,'ITI');
else
    % --- update tower visibility ---
    vr = towers_handling(vr,'updateVisibility');

    % --- update photoDiode plot ---
    % depends on towers_handling(vr,'updateVisibility') being executed first
    if vr.towers.towerJustAppeared == true
        vr.plot(1).color = [1 1 1];
    else
        vr.plot(1).color = [0 0 0];
    end
end


% --- prepare the next trial ---
if vr.tmp.endOfTrial == true
  
    % --- read GUI and update vr.S ---
    [vr.virmenGUI,vr.S] = virmen_updateGUI(vr.S,'sync',vr.virmenGUI);
    
    % --- execute callback functions for GUI parameters ---
    vr = virmen_GUICallback(vr);
    
    % --- take antiBias measure (if on) ---
    vr = towers_trials(vr,'antiBias');
    
    % --- log endOfTrial in scanimage tiff header, reset counter channels ---
    vr = towers_DAQ(vr,'endOfTrial');

    % --- update trialPlot ---
    vr = towers_trialPlot(vr,'update');
    
    % --- update performance plot ---
    vr = towers_performancePlot(vr,'update');
    
    % --- handle towers ---
    vr = towers_handling(vr,'makeAllInvisible');
    vr = towers_handling(vr,'prepareTrial');
    vr = towers_handling(vr,'placeTowers');
    
    % --- update trajectory plot ---
    vr = towers_trajectoryPlot(vr,'updateTrajectory'); 
    vr = towers_trajectoryPlot(vr,'updateTowers'); % must run after towers_handling(vr,'placeTowers')
    
    % --- update/reset variables, teleport mouse ---
    % this increments vr.tmp.currentTrial
    vr = towers_endOfTrial(vr,'endOfTrial');
end





% --- update movement variables ---
vr.mvt.previousCount = vr.mvt.currentCount;
vr.mvt.currentCount = vr.daq.ctr_session.inputSingleScan();

% --- keep track of how slow the non-virmen code is running ---
tmp_time = toc;
vr.tmp.executionInd = vr.tmp.executionInd + 1;
vr.tmp.executionTime(vr.tmp.executionInd) = tmp_time;

    
    
    
    
% -------------------------------------------------------------------
% -------------------------------------------------------------------
% --- TERMINATION code: executes after the ViRMEn engine stops.
% -------------------------------------------------------------------
% -------------------------------------------------------------------
function vr = terminationCodeFun(vr)


% --- date/time for filenames ---
vr.params.currentTime = datestr(now,'yymmdd_HHMMSS');

% --- clean up DAQ and log file ---
vr = towers_DAQ(vr,'cleanUp');

% --- read photoDiode binary temp file ---
nChannelsAcquired = length(vr.daq.ai_session.Channels); % to deinterleive correctly
fileID = fopen(vr.daq.logFile);
tmp_data = fread(fileID,'double');
fclose(fileID);
vr.photoDiode = reshape(tmp_data,nChannelsAcquired+1,[]); % +1 for timestamps

% --- save session figure as png ---
saveas(vr.myplots.figureHandle,...
       fullfile(vr.params.figureScreenshotPath,['fig_' vr.params.currentTime '.png']));

% --- close figures ---
close(vr.virmenGUI.ProtocolFigures.ParameterGUI);
close(vr.myplots.figureHandle);

% --- save data to file ---
d.data = vr.data;
d.params = vr.params;
d.tmp = vr.tmp;
d.mvt = vr.mvt;
save(fullfile(vr.params.savingPath,['data_' vr.params.currentTime '.mat']),'d');
disp('---saved data---')

% --- save settings ---
S = vr.S;
fname = [vr.params.mouse '_' vr.params.task '.mat'];
save(fullfile(vr.params.settingsPath,fname),'S');
disp('---saved settings---')





























