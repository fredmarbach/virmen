
function [position,movementType] = mvt_function_rotary(vr)

movementType = 'Position';

tmp_position = vr.daq.ctr_session.inputSingleScan();

position(1) = tmp_position;
position(2:4) = vr.position(2:4);