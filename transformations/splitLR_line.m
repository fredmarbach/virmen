% this function will split the view onto two monitors for a 'tunnel' view
% for each of N vertices, monitor x y coordinates will be determined
% 
% input:
%   - 3xN matrix with the current view of the mouse
% output:
%   - 3xNx2 matrix with (x,y) coordinates in the first 2 rows
%       the third row contains logicals describing visibility
%       the third dimension is for left and right monitor

function out2D = splitLR_line(coords3D,vr)

% --- define parameters ---
xFactor = vr.S.GUI.xFactor;
rFactor = vr.S.GUI.rFactor;
lengthScaleFactor = vr.S.GUI.lengthFactor;
monitorWidth = vr.S.GUI.monitorWidth * lengthScaleFactor;
monitorHeight = vr.S.GUI.monitorHeight * lengthScaleFactor;
mouseHeight = vr.S.GUI.mouseHeight;
minDistance_forX = vr.S.GUI.minDistX * lengthScaleFactor;
monitorAngle = vr.S.GUI.monitorAngle * lengthScaleFactor;
minDistance_forY = vr.S.GUI.minDistY * lengthScaleFactor;

aspectRatio = monitorWidth/monitorHeight;
d = minDistance_forX / sind(monitorAngle);


% --- initialise ---
% out2D is 3 (xy visibility) x nVertices x 2 (monitors)
out2D = ones(3,size(coords3D,2),2); % set all points to visible

above = coords3D(3,:) >= 0;
below = coords3D(3,:) < 0;
right = coords3D(1,:) > 0;
left = coords3D(1,:) < 0;


% --- transform x coordinates ---
slope1 = coords3D(2,:) ./ coords3D(1,:); % slope of line through mouse
slope2_left = 1 / tand(monitorAngle); % slope of left monitor
slope2_right = -1 / tand(monitorAngle); % slope of right monitor

% --- easier to understand but heavier version ---
% intersect_x = zeros(1,size(coords3D,2));
% tmp = slope1(left)-slope2_left;
% tmp(isinf(tmp)) = 10e-10;
% intersect_x(left) = d ./ tmp; 
% tmp = slope1(right)-slope2_right;
% tmp(isinf(tmp)) = 10e-10;
% intersect_x(right) = d ./ tmp;  
% new_x = abs(intersect_x) / sind(monitorAngle);
% new_x_norm = new_x / (0.5*monitorHeight); % monitor x coordinates normalised and positive

slope2 = zeros(1,length(slope1));
slope2(left) = slope2_left;
slope2(right) = slope2_right;
tmp = slope1 - slope2;
tmp(isinf(tmp)) = 10e-10;
intersect_x = d ./ tmp;  
new_x_norm = (abs(intersect_x) / sind(monitorAngle)) / (0.5*monitorHeight);

out2D(1,:,1) = -new_x_norm + aspectRatio;
out2D(1,:,2) = new_x_norm - aspectRatio;


% --- set x boundaries ---

% X left monitor
% for now, allowed values go from -2*aspectRatio to 0
ignoreLeft_r = out2D(1,:,1) > aspectRatio | right | intersect_x > 0;
ignoreLeft_l = out2D(1,:,1) < -aspectRatio;
out2D(1,ignoreLeft_l,1) = -aspectRatio;
out2D(1,ignoreLeft_r,1) = aspectRatio;
out2D(2,((ignoreLeft_l | ignoreLeft_r) & above),1) = 1;
out2D(2,((ignoreLeft_l | ignoreLeft_r) & below),1) = -1;

% X right monitor
ignoreRight_l = out2D(1,:,2) < -aspectRatio | left | intersect_x < 0;
ignoreRight_r = out2D(1,:,2) > aspectRatio;
out2D(1,ignoreRight_r,2) = aspectRatio;
out2D(1,ignoreRight_l,2) = -aspectRatio;
out2D(2,((ignoreRight_l | ignoreRight_r) & above),2) = 1;
out2D(2,((ignoreRight_l | ignoreRight_r) & below),2) = -1;

% set ignored points to invisible
out2D(3,(ignoreLeft_l | ignoreLeft_r),1) = 0;
out2D(3,(ignoreRight_l | ignoreRight_r),2) = 0;


% --- transform y coordinates ---

radialDistance = sqrt((xFactor*coords3D(1,:)).^2 + coords3D(2,:).^2);

% --- easier to understand but heavier version ---
% new_y = minDistance_forY * coords3D(3,:)./(rFactor*radialDistance); % z/y
% new_y_norm = new_y / (0.5*monitorHeight); % normalise to monitor coordinates
% new_y_norm_shift = new_y_norm - 1 + mouseHeight;

new_y_norm_shift = ((minDistance_forY * coords3D(3,:)./(rFactor*radialDistance))...
              / (0.5*monitorHeight)) - 1 + mouseHeight; % normalise to monitor coordinates
% assign new y coordinates
out2D(2,:,1) = new_y_norm_shift;
out2D(2,:,2) = new_y_norm_shift;


% --- set y boundaries ---

ignoreBottom = new_y_norm_shift < -1;
ignoreTop = new_y_norm_shift > 1;

% Y left and right monitor
out2D(2,ignoreBottom,:) = -1;
out2D(2,ignoreTop,:) = 1;
out2D(3,ignoreBottom,:) = 0;
out2D(3,ignoreTop,:) = 0;

%tmp = coords3D(1,:) < 0;
%plot(x(tmp))
% figure
% tmp = out2D(3,:,1)==1;
% scatter(out2D(1,tmp,1),out2D(2,tmp,1))




  
  