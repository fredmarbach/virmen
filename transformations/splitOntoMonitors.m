% this function will split the view onto two monitors for a 'tunnel' view
% for each of N vertices, monitor x y coordinates will be determined
% 
% input:
%   - 3xN matrix with the current view of the mouse
% output:
%   - 3xNx2 matrix with (x,y) coordinates in the first 2 rows
%       the third row contains logicals describing visibility
%       the third dimension is for left and right monitor

function coords2D = splitOntoMonitors(coords3D)



monitorAngle = 35;
minDistance = 2200; % smallest distance from monitor to mouse in mm
monitorWidth = 556; % width of monitor in mm
monitorHeight = 400; % height of monitor in mm
aspectRatio = monitorWidth/monitorHeight;
%aspectRatio = 4:3;
s = 1;
p = 1;

coords2D = zeros(size(coords3D,1),size(coords3D,2));
% by default, make all points visible
coords2D(3,:) = true;

%radialDistance = sqrt(coords3D(2,:).^2 + coords3D(1,:).^2);
coords2D(1,:) = s * coords3D(1,:)./coords3D(2,:); % x/y
coords2D(2,:) = s * coords3D(3,:)./coords3D(2,:); % z/y


for ii = 1:size(coords2D,2)
        
    if coords3D(2,ii) < 0 % 3D point behind mouse
        coords2D(3,ii) = 0;
        if coords3D(1,ii) > 0
            xSign = 1;
        else
            xSign = -1;
        end
        if coords3D(3,ii) > 0
            zSign = 1;
        else
            zSign = -1;
        end
        coords2D(1,ii) = p * xSign * aspectRatio;
        coords2D(2,ii) = p * zSign;
    else
        coords2D(3,ii) = 1;
    end
end

% figure
% scatter(coords2D(1,:),coords2D(2,:))

out2D = coords2D;

% left monitor
% ignoreLeft = out2D(1,:) >= 0;
% out2D(3,ignoreLeft) = 0;
% out2D(1,ignoreLeft) = 0;
% out2D(1,:) = out2D(1,:) * 2;




% % initialise output
% % 3 (x,y,visible) x N x 2 (monitors)
% coords2D = zeros(size(coords3D,1),size(coords3D,2),2);
% % by default, make all points visible
% coords2D(3,:,:) = true;
% 
% coords2D(1,:,1) = s * coords3D(1,:)./coords3D(2,:); % x/y
% coords2D(2,:,1) = s * coords3D(3,:)./coords3D(2,:); % z/y
% coords2D(1,:,2) = coords2D(1,:,1); % x/y
% coords2D(2,:,2) = coords2D(2,:,1); % z/y
% 
% ignoreY = abs(coords2D(2,:,1)) > 1;
% coords2D(3,ignoreY,:) = false;
% 
% % all negative x coordinates go on left monitor
% ignoreLeft = coords2D(1,:,1) > 0;
% coords2D(3,ignoreLeft,1) = false;
% 
% ignoreRight = coords2D(1,:,2) > 0;
% coords2D(3,ignoreRight,2) = false;

% normalise to monitor coordinates
%coords2D(1:2,:,1) = coords2D(1:2,:,1)/(0.5*monitorHeight);

% shift onto left monitor
%coords2D(1,:,1) = coords2D(1,:,1) + aspectRatio;

%tmp = coords2D(3,:,1)==1;
%scatter(coords2D(1,:,1),coords2D(2,:,1))
% scatter(coords2D(1,tmp,1),coords2D(2,tmp,1))


% his example code

% % define constants that describe setup geometry
% alpha = 2.0349;
% beta = -0.98988;
% % create an output matrix of the same size as the input
% % first two rows are x and y
% % the third row indicates whether the location should be visible
% coords2D = zeros(size(coords3D));
% % by default, make all points visible
% coords2D(3,:) = true;
% % calculate radius
% r = sqrt(coords3D(1,:).^2 + coords3D(2,:).^2);
% % calculate a scaling factor
% s = 1./(alpha*r + beta*coords3D(3,:));
% % if a point is outside of the screen, set the scaling factor such that the
% % point is plotted at the edge of the screen, and make the point invisible
% % (if all 3 vertices of a triangle are invisible, it is not plotted)
% f = find(s < 0 | r.*s > 1);
% s(f) = 1./r(f);
% coords2D(3,f) = false;
% % calculate x and y components using the scaling factor
% coords2D(1,:) = s.*coords3D(1,:);
% coords2D(2,:) = s.*coords3D(2,:);
  
  
  