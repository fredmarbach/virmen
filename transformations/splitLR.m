% this function will split the view onto two monitors for a 'tunnel' view
% for each of N vertices, monitor x y coordinates will be determined
% 
% input:
%   - 3xN matrix with the current view of the mouse
% output:
%   - 3xNx2 matrix with (x,y) coordinates in the first 2 rows
%       the third row contains logicals describing visibility
%       the third dimension is for left and right monitor

function out2D = splitLR(coords3D)

% --- intialise ---

scaleFactor = 1;
monitorWidth = 556 * scaleFactor; % width of monitor in mm
monitorHeight = 400 * scaleFactor; % height of monitor in mm
aspectRatio = monitorWidth/monitorHeight;
%aspectRatio = 4:3;
mouseHeight = 3.0 / (2*monitorHeight); % in monitor coordinates from bottom of monitor
monitorAngle = 35;
minDistance = 220 * scaleFactor; % smallest distance from monitor to mouse in mm
minDistance_yFactor = 220 * scaleFactor; % distance from monitor for y scaling; might want to cheat with this
d = minDistance / sind(monitorAngle);

out2D = zeros(size(coords3D,1),size(coords3D,2),2);
% by default, make all points visible
out2D(3,:,:) = 1;


% --- transform x coordinates ---
%tanTheta = tan(coords3D(1,:) ./ coords3D(2,:));
tanTheta = tan(abs(coords3D(1,:)) ./ abs(coords3D(2,:)));
tanAlpha = tand(monitorAngle);
x = (tanTheta*tanAlpha*d) ./ (sind(monitorAngle)*(tanTheta+tanAlpha)); % monitor x coordinates in mm
x_norm = x / (0.5*monitorHeight); % monitor x coordinates normalised
out2D(1,:,1) = x_norm; % assign new x coordinates
out2D(1,:,2) = x_norm; % assign new x coordinates


% --- set x boundaries ---

above = coords3D(3,:) >= 0;
below = coords3D(3,:) < 0;

% X left monitor
% for now, allowed values go from -2*aspectRatio to 0
ignoreLeft_r = x_norm >= 0;
ignoreLeft_l = x_norm < -2*aspectRatio;
out2D(1,ignoreLeft_l,1) = -2*aspectRatio;
out2D(1,ignoreLeft_r,1) = 0;
out2D(2,((ignoreLeft_l | ignoreLeft_r) & above),1) = 2-mouseHeight;
out2D(2,((ignoreLeft_l | ignoreLeft_r) & below),1) = -mouseHeight;

% X right monitor
ignoreRight_l = x_norm < 0;
ignoreRight_r = x_norm > 2*aspectRatio;
out2D(1,ignoreRight_r,2) = 2*aspectRatio;
out2D(1,ignoreRight_l,2) = 0;
out2D(2,((ignoreRight_l | ignoreRight_r) & above),2) = 2-mouseHeight;
out2D(2,((ignoreRight_l | ignoreRight_r) & below),2) = -mouseHeight;

% set ignored points to invisible
out2D(3,(ignoreLeft_l | ignoreLeft_r),:) = 0;
% add offset to centre on monitor
out2D(1,:,1) = out2D(1,:,1) + aspectRatio; % currently from -2*aspectRatio to 0 -- shift to the right
out2D(1,:,2) = out2D(1,:,2) - aspectRatio; % currently from 0 to 2*aspectRatio -- shift to the left


% --- transform y coordinates ---

y = minDistance_yFactor * coords3D(3,:)./abs(coords3D(2,:)); % z/y
y_norm = y / (0.5*monitorHeight);
% assign new y coordinates
out2D(2,:,1) = y_norm;
out2D(2,:,2) = y_norm;


% --- set y boundaries ---

ignoreBottom = y_norm < -mouseHeight;
ignoreTop = y_norm > 2-mouseHeight;

out2D(3,(ignoreBottom | ignoreTop),:) = 0;
% Y left and right monitor
out2D(2,ignoreBottom,:) = -mouseHeight;
out2D(2,ignoreTop,:) = 2-mouseHeight;
% add offset to centre on monitor
out2D(2,:,:) = out2D(2,:,:) - 1 + mouseHeight;


tmp = coords3D(1,:) < 0;
plot(x(tmp))
% figure
% tmp = out2D(3,:,1)==1;
% scatter(out2D(1,tmp,1),out2D(2,tmp,1))
% axis tight


% his example code

% % define constants that describe setup geometry
% alpha = 2.0349;
% beta = -0.98988;
% % create an output matrix of the same size as the input
% % first two rows are x and y
% % the third row indicates whether the location should be visible
% coords2D = zeros(size(coords3D));
% % by default, make all points visible
% coords2D(3,:) = true;
% % calculate radius
% r = sqrt(coords3D(1,:).^2 + coords3D(2,:).^2);
% % calculate a scaling factor
% s = 1./(alpha*r + beta*coords3D(3,:));
% % if a point is outside of the screen, set the scaling factor such that the
% % point is plotted at the edge of the screen, and make the point invisible
% % (if all 3 vertices of a triangle are invisible, it is not plotted)
% f = find(s < 0 | r.*s > 1);
% s(f) = 1./r(f);
% coords2D(3,f) = false;
% % calculate x and y components using the scaling factor
% coords2D(1,:) = s.*coords3D(1,:);
% coords2D(2,:) = s.*coords3D(2,:);
  



% 0 angle approach that failed:


% tmpX = s * coords3D(2,:)./abs(coords3D(1,:)); % y/x
% tmpY = s * coords3D(3,:)./radialDistance; % z/y
% % left monitor
% out2D(1,:,1) = tmpX;
% out2D(2,:,1) = tmpY;
% % right monitor
% out2D(1,:,2) = tmpX;
% out2D(2,:,2) = -tmpY;

% backLeft = coords3D(1,:) < 0 | coords3D(2,:) < 0;
% backRight = coords3D(1,:) >= 0 | coords3D(2,:) < 0;
% frontLeft = coords3D(1,:) < 0 | coords3D(2,:) >= 0;
% frontRight = coords3D(1,:) >= 0 | coords3D(2,:) >= 0;
% above = coords3D(3,:) >= 0;
% below = coords3D(3,:) < 0;

% left monitor
% out2D(3,(backRight | frontRight),1) = 0;
% out2D(1,backRight,1) = -p * aspectRatio;
% out2D(1,frontRight,1) = p * aspectRatio;
% out2D(2,(backRight | frontRight) & below,1) = -p;
% out2D(2,(backRight | frontRight) & above,1) = p;


% right monitor
% out2D(3,(backLeft | frontLeft),2) = 0;
% out2D(1,backLeft,2) = -p * aspectRatio;
% out2D(1,frontLeft,2) = p * aspectRatio;
% out2D(2,(backLeft | frontLeft) & below,2) = -p;
% out2D(2,(backLeft | frontLeft) & above,2) = p;
  
  